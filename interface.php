<?php
interface RBI{
   function deposite($deposite_amt);
   function withdraw($withdraw_amt);

}
class SBI implements RBI{

    public function __construct($bal){
        $this->balance = $bal;
    } 
    function deposite($deposite_amt){
        $this->balance = $this->balance + $deposite_amt;
        echo $this.PHP_EOL;

    }
    function withdraw($withdraw_amt){
        $this->balance = $this->balance - $withdraw_amt;
        echo $this.PHP_EOL;
    }
    public function __toString(){
        return "current balance"." ".$this->balance;
    }
}

    $obje = new SBI(1000);
    //echo $obje;
    $obje->withdraw(200);
    //echo $obje;
    $obje->deposite(500);
?>